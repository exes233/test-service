FROM golang:1.18-alpine as buildbase

RUN apk add git build-base

WORKDIR /go/src/new-service
COPY vendor .
COPY . .

RUN GOOS=linux go build  -o /usr/local/bin/new-service /go/src/new-service


FROM alpine:3.9

COPY --from=buildbase /usr/local/bin/new-service /usr/local/bin/new-service
RUN apk add --no-cache ca-certificates

ENTRYPOINT ["new-service"]
