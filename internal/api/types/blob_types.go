package types

type BlobType int32

const (
	BlobTypeAssetDescription BlobType = 1 << iota
	BlobTypeFundOverview
	BlobTypeFundUpdate
	BlobTypeNavUpdate
	BlobTypeFundDocument
	BlobTypeAlpha
	BlobTypeBravo
	BlobTypeCharlie
	BlobTypeDelta
	BlobTypeTokenTerms
	BlobTypeTokenMetrics
	BlobTypeKYCForm
	BlobTypeKYCIdDocument
	BlobTypeKYCPoa
	BlobTypeIdentityMindReject
)

func IsPublicBlob(t BlobType) bool {
	return t <= BlobTypeTokenMetrics
}
