package postgres

import (
	"database/sql"

	"github.com/lann/squirrel"
	"github.com/lib/pq"
	"github.com/pkg/errors"
	"gitlab.com/tokend/api/db2"
	"gitlab.com/test-service/internal/api/data"
	"gitlab.com/test-service/internal/api/types"
)

const (
	blobsTable        = "blobs"
	blobsPKConstraint = "blobs_pkey"
)

var (
	ErrBlobsConflict = errors.New("blobs primary key conflict")
	ErrNoWallet      = errors.New("blobs_wallets_fkey violated")
	blobsSelect      = squirrel.
				Select("id", "owner_address", "value", "type", "creator_signer_role").
				From(blobsTable)
)

type Blobs struct {
	*db2.Repo
	stmt squirrel.SelectBuilder
}

func NewBlobs(repo *db2.Repo) *Blobs {
	return &Blobs{
		repo.Clone(), blobsSelect,
	}
}

func (q *Blobs) New() data.Blobs {
	return NewBlobs(q.Repo.Clone())
}

func (q *Blobs) Transaction(fn func(data.Blobs) error) error {
	return q.Repo.Transaction(func() error {
		return fn(q)
	})
}

func (q *Blobs) Create(blob *types.Blob) error {
	stmt := squirrel.Insert(blobsTable).SetMap(map[string]interface{}{
		"owner_address":       *blob.OwnerAddress,
		"id":                  blob.ID,
		"type":                blob.Type,
		"value":               blob.Value,
		"creator_signer_role": blob.CreatorSignerRole,
	})

	_, err := q.Exec(stmt)
	if err != nil {
		cause := errors.Cause(err)
		pqerr, ok := cause.(*pq.Error)
		if ok {
			if pqerr.Constraint == blobsPKConstraint {
				return ErrBlobsConflict
			}
		}
	}
	return err
}

func (q *Blobs) Get(id string) (*types.Blob, error) {
	var result types.Blob
	stmt := q.stmt.Where("id = ?", id)

	err := q.Repo.Get(&result, stmt)
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, nil
		}
		return nil, err
	}
	return &result, nil
}
func (q *Blobs) Delete(id string) error {
	stmt := q.stmt.Where("id = ?", id)
	err := q.Repo.Delete(&result, stmt)
	if err != nil {
		if err == sql.ErrNoRows {
			return nil
		}
		return err
	}
	return nil
}
func (q *Blobs) GetAll() (data.Blobs, error) {
	var result data.Blobs
	[]result := q.Select("*").From(blobsTable)
	if result == nil {
		return nil, sql.ErrNoRows
	}
	return result, nil
}
