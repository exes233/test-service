package handlers

import (
	"net/http"

	"github.com/go-chi/chi"
	. "github.com/go-ozzo/ozzo-validation"
	"gitlab.com/distributed_lab/ape"
	"gitlab.com/distributed_lab/ape/problems"
	"gitlab.com/test-service/internal/api/handlers/requests"
	"gitlab.com/test-service/internal/api/resources"
	"gitlab.com/test-service/internal/api/data"
	"gitlab.com/test-service/internal/api/types"
	"gitlab.com/tokend/go/doorman"
)
func DeleteBlob(w http.ResponseWriter, r *http.Request){
	request, err := NewDeleteBlobRequest(r)
	if err != nil {
		ape.RenderErr(w, problems.BadRequest(err)...)
		return
	}
	err := BlobQ(r).Delete(request.BlobID)
	if err != nil {
		ape.RenderErr(w, problems.InternalError())
		return
	}
}