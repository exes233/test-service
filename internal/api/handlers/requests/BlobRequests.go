package requests

import (
	"encoding/base32"
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/go-chi/chi"
	validation "github.com/go-ozzo/ozzo-validation"
	"gitlab.com/distributed_lab/logan/v3/errors"
	"gitlab.com/test-service/internal/api/data"
	"gitlab.com/test-service/internal/api/resources"
	"gitlab.com/tokend/go/hash"
)

func NewCreateBlobRequest(r *http.Request) (resources.BlobRequest, error) {
	var request resources.BlobRequestResponse

	if err := json.NewDecoder(r.Body).Decode(&request); err != nil {
		return request.Data, errors.Wrap(err, "failed to unmarshal")
	}

	if request.Data.Relationships.Owner.Data == nil {
		request.Data.Relationships.Owner.Data = &resources.Key{ID: chi.URLParam(r, "address")}
	}

	return request.Data, ValidateCreateBlobRequest(request.Data)
}

func ValidateCreateBlobRequest(r resources.BlobRequest) error {
	return validation.Errors{
		"/data/type":                        validation.Validate(&r.Type, validation.Required),
		"/data/attributes/value":            validation.Validate(&r.Attributes.Value, validation.Required),
		"/data/relationships/owner/data/id": validation.Validate(&r.Relationships.Owner.Data.ID, validation.Required),
	}.Filter()
}

func Blob(r resources.BlobRequest, allowedRole uint64) (*types.Blob, error) {
	blob, err := types.GetBlobType(string(r.Type))
	if err != nil {
		return nil, errors.Wrap(err, "failed to create blob")
	}
	msg := fmt.Sprintf("%s%d%s", r.Relationships.Owner.Data.ID, blob, r.Attributes.Value)
	hash := hash.Hash([]byte(msg))
	owner := types.Address(r.Relationships.Owner.Data.ID)

	return &types.Blob{
		ID:                base32.StdEncoding.WithPadding(base32.NoPadding).EncodeToString(hash[:]),
		Type:              blob,
		Value:             r.Attributes.Value,
		CreatorSignerRole: &allowedRole,
		OwnerAddress:      &owner,
	}, nil
}
type (
	GetBlobRequest struct {
		Address types.Address `json:"-"`
		BlobID  string        `json:"-"`
	}
)

func NewGetBlobRequest(r *http.Request) (GetBlobRequest, error) {
	request := GetBlobRequest{
		Address: types.Address(chi.URLParam(r, "address")),
		BlobID:  chi.URLParam(r, "blob"),
	}
	return request, request.Validate()
}

func (r GetBlobRequest) Validate() error {
	err := Errors{
		"blob": Validate(&r.BlobID, Required),
	}
	return err.Filter()
}
type (
	DeleteBlobRequest struct {
		Address types.Address `json:"-"`
		BlobID  string        `json:"-"`
	}
)
func NewDeleteBlobRequest(r *http.Request) (DeleteBlobRequest, error) {
	request := DeleteBlobRequest{
		Address: types.Address(chi.URLParam(r, "address")),
		BlobID:  chi.URLParam(r, "blob"),
	}
	return request, request.Validate()
}



