package handlers

import (
	"fmt"
	"net/http"

	"github.com/google/jsonapi"
	"gitlab.com/tokend/go/signcontrol"

	validation "github.com/go-ozzo/ozzo-validation"
	"github.com/pkg/errors"
	"gitlab.com/distributed_lab/ape"
	"gitlab.com/distributed_lab/ape/problems"
	"gitlab.com/test-service/internal/api/handlers/requests"
	"gitlab.com/test-service/internal/api/resources"
	"gitlab.com/test-service/internal/api/data"
	"gitlab.com/test-service/internal/api/types"
)

const(
	badRequestFictiveRole   = 400
	unauthorizedFictiveRole = 401
	forbiddenFictiveRole    = 403
	internalErrorFictiveRole = 500
)

func CreateBlob(w http.ResponseWriter, r *http.Request) {
	request, err := requests.NewCreateBlobRequest(r)
	if err != nil {
		ape.RenderErr(w, problems.BadRequest(err)...)
		return
	}
	role, err := getSignerRole(r, request.Relationships.Owner.Data.ID)
	if err != nil {
		switch role {
		case badRequestFictiveRole:
			ape.RenderErr(w, &jsonapi.ErrorObject{
				Title:  http.StatusText(http.StatusBadRequest),
				Status: fmt.Sprint(http.StatusBadRequest),
				Detail: "Request signature was invalid in some way",
			})
		case unauthorizedFictiveRole:
			ape.RenderErr(w, problems.NotAllowed())
		case forbiddenFictiveRole:
			ape.RenderErr(w, problems.Forbidden())
		default:
			ape.RenderErr(w, problems.InternalError())
		}
		return
	}

	blob, err := requests.Blob(request, role)
	if err != nil {
		ape.RenderErr(w, problems.BadRequest(validation.Errors{"/data/type": errors.New("invalid blob type")})...)
		return
	}

	err = BlobQ(r).Transaction(func(blobs data.Blobs) error {
		if err := blobs.Create(blob); err != nil {
			return errors.Wrap(err, "failed to create blob")
		}
		return nil
	})
	if err != nil {
		if errors.Cause(err) != postgres.ErrBlobsConflict {
			ape.RenderErr(w, problems.InternalError())
			return
		}
	}

	response := resources.BlobResponse{
		Data: NewBlob(blob),
	}
	w.WriteHeader(201)
	ape.Render(w, &response)
}

func getSignerRole(r *http.Request, ownerAddress string) (uint64, error) {
	signer, err := signcontrol.CheckSignature(r)
	if err != nil {
		return badRequestFictiveRole, errors.Wrap(err, "bad signature")
	}

	findSigner := func(_signer, address string) (uint64, error) {
		accountSigners, err := AccountQ(r).Signers(address)
		if err != nil {
			return internalErrorFictiveRole, errors.Wrap(err, "failed to get owner signers")
		}
		if accountSigners == nil {
			return forbiddenFictiveRole, errors.New("account '" + address + "' does not have any signers")
		}
		for _, s := range accountSigners {
			if s.AccountID == _signer {
				return s.Role, nil
			}
		}
		return unauthorizedFictiveRole, errors.New("signer not found in account signers")
	}

	result, err := findSigner(signer, CoreInfo(r).GetMasterAccountID())
	if err == nil {
		return result, nil
	}
	return findSigner(signer, ownerAddress)
}
