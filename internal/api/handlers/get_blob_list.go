package handlers

import (
	"net/http"

	"github.com/go-chi/chi"
	. "github.com/go-ozzo/ozzo-validation"
	"gitlab.com/distributed_lab/ape"
	"gitlab.com/distributed_lab/ape/problems"
	"gitlab.com/test-service/internal/api/handlers/requests"
	"gitlab.com/test-service/internal/api/resources"
	"gitlab.com/test-service/internal/api/data"
	"gitlab.com/test-service/internal/api/types"
	"gitlab.com/tokend/go/doorman"
)
func GetBlobList(w http.ResponseWriter, r *http.Request){
	[]blob, err := BlobQ(r).GetAll()
	if err != nil {
		ape.RenderErr(w, problems.InternalError())
		return
	}
	if blob == nil {
		ape.RenderErr(w, problems.NotFound())
		return
	}
	response := resources.BlobResponse{
		Data: NewBlobs(blob),
	}
	ape.Render(w, &response)
}