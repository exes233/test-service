package handlers

import (
	"net/http"

	"github.com/go-chi/chi"
	. "github.com/go-ozzo/ozzo-validation"
	"gitlab.com/distributed_lab/ape"
	"gitlab.com/distributed_lab/ape/problems"
	"gitlab.com/test-service/internal/api/handlers/requests"
	"gitlab.com/test-service/internal/api/resources"
	"gitlab.com/test-service/internal/api/data"
	"gitlab.com/test-service/internal/api/types"
	"gitlab.com/tokend/go/doorman"
)
func GetBlob(w http.ResponseWriter, r *http.Request) {
	request, err := NewGetBlobRequest(r)
	if err != nil {
		ape.RenderErr(w, problems.BadRequest(err)...)
		return
	}

	blob, err := BlobQ(r).Get(request.BlobID)
	if err != nil {
		ape.RenderErr(w, problems.InternalError())
		return
	}

	if blob == nil {
		ape.RenderErr(w, problems.NotFound())
		return
	}

	if !types.IsPublicBlob(blob.Type) {
		constrains := []doorman.SignerConstraint{doorman.SignerOf(CoreInfo(r).GetMasterAccountID())}

		if blob.OwnerAddress != nil {
			ownerAddress := blob.OwnerAddress.String()

			kycRole, err := SystemSettings(r).KYCRecoverySignerRole()
			if err != nil {
				ape.RenderErr(w, problems.InternalError())
				return
			}

			constrain := doorman.SignerOf(ownerAddress)

			if blob.CreatorSignerRole != nil && *blob.CreatorSignerRole == kycRole {
				constrain = doorman.ClearSignerOf(ownerAddress)
			}

			constrains = append(constrains, constrain)
		}
		if err := Doorman(r, constrains...); err != nil {
			ape.RenderErr(w, problems.NotAllowed(err))
			return
		}
	}
	response := resources.BlobResponse{
		Data: NewBlob(blob),
	}

	ape.Render(w, &response)
}

func NewBlob(blob *types.Blob) resources.Blob {
	b := resources.Blob{
		Key: resources.Key{
			ID:   blob.ID,
			Type: resources.ResourceType(blob.Type.String()),
		},
		Attributes: resources.BlobAttributes{
			Value: blob.Value,
		},
	}
	return b
}
